# Technical Assessment

## Installation

> "npm install"

## Run Local

> npm run dev

## Try Demo

> https://softwareseni.herokuapp.com without port

## Postman / Documentation

> https://documenter.getpostman.com/view/3713115/Tz5iBMRP
