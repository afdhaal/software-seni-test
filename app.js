const express = require("express");
const app = express();
const bodyParser = require("body-parser");
//checker running env
const initializeEnv = require("./utils/initializeEnv");
const port = process.env.PORT || 3000;
var cors = require("cors");

app.use(cors());
app.use(bodyParser.json({limit: "50mb"}));
app.use(
	bodyParser.urlencoded({
		limit: "50mb",
		extended: true,
	}),
);

const article = require("./modules/articles/routes/article");
const book = require("./modules/books/routes/books");
app.use("/article", article);
app.use("/book", book);

app.get("/", (req, res) => {
	res.send("Hello World!");
});

app.listen(port, () => {
	console.log("Example app listening on port port! " + port);
});
