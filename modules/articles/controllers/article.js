const axios = require("axios");
const Joi = require("joi");

exports.search = async (req, res, next) => {
	console.log("jos");
	let page = req.query.page;
	let sort = req.query.sort;

	if (!page) {
		page = 0;
	}
	console.log(page);
	const schema = Joi.object({
		search: Joi.string().required(),
	});
	const options = {
		abortEarly: false, // include all errors
		allowUnknown: true, // ignore unknown props
		stripUnknown: true, // remove unknown props
	};

	// validate request body against schema
	const {error, value} = schema.validate(req.query, options);

	if (error) {
		// on fail return comma separated errors
		const response = {
			error: true,
			message: error.details.map(x => x.message).join(", "),
			data: {},
		};
		res.status(400).send(response);
	} else {
		let config = {
			headers: {"Content-Type": "application/json"},
			params: {
				"api-key": process.env.API_KEY_NYT,
				q: req.query.search,
				page: page,
				sort: sort,
			},
		};

		//search article from nyt
		axios
			.get("https://api.nytimes.com/svc/search/v2/articlesearch.json", config)
			.then(data => {
				//mapping response
				const response = {
					error: false,
					message: "Data Article",
					data: data.data.response.docs.map(
						({
							_id,
							byline,
							headline,
							web_url,
							uri,
							snippet,
							type_of_material,
							multimedia,
							pub_date,
						}) => ({
							_id,
							byline,
							headline,
							detail: web_url,
							uri,
							snippet,
							type_of_material,
							pub_date,
							multimedia,
						}),
					),
				};
				res.send(response);
			})
			.catch(function (error) {
				// console.log(error);
				const response = {
					error: true,
					message: error,
					data: {},
				};
				res.status(400).send(response);
			});
	}
};
