const express = require("express");
const articleController = require("../../articles/controllers/article");
const router = express.Router();

router.get("/search", articleController.search);

module.exports = router;
