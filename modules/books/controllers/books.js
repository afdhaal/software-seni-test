const axios = require("axios");
const Joi = require("joi");

exports.search = async (req, res, next) => {
	const schema = Joi.object({
		list: Joi.string().required(),
	});
	const options = {
		abortEarly: false, // include all errors
		allowUnknown: true, // ignore unknown props
		stripUnknown: true, // remove unknown props
	};

	// validate request body against schema
	const {error, value} = schema.validate(req.query, options);

	if (error) {
		// on fail return comma separated errors
		const response = {
			error: true,
			message: error.details.map(x => x.message).join(", "),
			data: {},
		};
		res.status(400).send(response);
	} else {
		let config = {
			headers: {"Content-Type": "application/json"},
			params: {
				"api-key": process.env.API_KEY_NYT,
				list: req.query.list,
			},
		};

		//search books from nyt
		axios
			.get("https://api.nytimes.com/svc/books/v3/lists.json", config)
			.then(data => {
				//mapping response
				const response = {
					error: false,
					message: "Data Books",
					data: data.data.results.map(
						({
							published_date,
							book_details,
							amazon_product_url,
							list_name,
							bestsellers_date,
							rank,
							rank_last_week,
							weeks_on_list,
							asterisk,
							dagger,
						}) => ({
							published_date,
							book_details,
							amazon_product_url,
							list_name,
							bestsellers_date,
							rank,
							rank_last_week,
							weeks_on_list,
							asterisk,
							dagger,
						}),
					),
				};
				res.send(response);
			})
			.catch(function (error) {
				console.log(error);
				const response = {
					error: true,
					message: error,
					data: {},
				};
				res.status(400).send(response);
			});
	}
};
