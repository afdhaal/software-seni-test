const express = require("express");
const bookController = require("../../books/controllers/books");
const router = express.Router();

router.get("/search", bookController.search);

module.exports = router;
